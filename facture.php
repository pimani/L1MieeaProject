<?php
session_start();

if(!isset($_SESSION['id']) || $_SESSION['id'] == ""){
    header("Location: 404.php");
    exit();
}

$ini_array = parse_ini_file("php/conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");

$from = isset($_POST['link']) && $_POST['link'] != "" ? $_POST['link'] : "404.php";


if (!$connexion) {
    // Vérifie que la connexion à la base c'est bien passer
    mysqli_close($connexion);
    header("Location: $from?réussite=Base_de_données_inaccessible");
    exit();
}

function accessData($res, $co, $fr)
{
    // Exécute la requête sur la base de donnée et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if (!$res) {
        mysqli_close($co);
        header("Location: $fr?réussite=Erreur_base_de_données");
        exit();
    }
    return $res;
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <title>Facture</title>
</head>
<body>
<header>
    <p>
        Toutes les factures sont fictives, cette fiche est générée seulement à des fins d'exercices.
    </p>
</header>
<?php
    $date = getdate();
    $good = TRUE;
    $name = "Facture/Facture_" . $_SESSION['id'] . "_" . $date[0] . ".txt";

    $fp = fopen($name, "w");
    if ($fp == NULL) {
        $good = FALSE;
        echo "<p>La facture ne poura pas être créer sour forme de fichier</p>";
    } else {
        if (!fwrite($fp, "Toutes les factures sont fictives, cette fiche est générée seulement à des fins d'exercices.\n\n --------------------------------------------------\n")) {
            $good = FALSE;
        }
    }

    $request = "SELECT * FROM command WHERE buyer='" . $_SESSION["id"] . "'";
    $request = accessData($request, $connexion, $from);

    if (mysqli_num_rows($request) == 0) {
        echo "<p>Vous n'avez commandé aucun produit.<p/>";
    }

    $totalNb = 0;
    $totalPrice = 0;

    echo "<h4>Produits :</h4>";
    echo "<table>";
    for ($i = 0; $i < mysqli_num_rows($request); $i++) {
        $data = mysqli_fetch_array($request);

        $livre = "SELECT * FROM produit WHERE id='" . $data['book'] . "'";
        $livre = accessData($livre, $connexion, $from);
        $livre = mysqli_fetch_array($livre);

        $totalNb = $totalNb + $data['nb'];
        $totalPrice = $totalPrice + $livre['price']*$data['nb'];

        echo "<tr><td>" . $livre['book'] . "</td><td>" . $data['nb'] . "</td><td>" . $livre['price'] . " €</td></tr>";
        if ($good != FALSE) {
            if (!fwrite($fp, "" . $livre['book'] . "\t" . $data['nb'] . "\t" . $livre['price'] . "euro\n")) {
                $good = FALSE;
            }
        }
    }


    echo "<tr><td>Total :</td><td>$totalNb</td><td>" . $totalPrice . " €</td></tr>";
    if (!fwrite($fp, "Total \t" . $totalNb . " \t " . $totalPrice . " €\n")) {
        $good = FALSE;
    }
    echo "</table>";

    $text = "\n\nMentions légales:\n\nPas d'escompte pour paiement anticipé.Conformément à la loi 92.1142, en cas de retard de paiement, toute somme, y compris l'acompte, non payée à sa date d'éxigibilité produira de plein droit des intérêts de retard équivalents au triple du taux d'intérêt légal de l'année en cours ainsi que le paiement d'une somme forfaitaire de quarante (40) euros due au titre des frais de recouvrement conformément au décret N°2012-1115.";
    if (!fwrite($fp, $text)) {
        $good = FALSE;
    }

    fclose($fp);
?>

<div id="mentions_légales">
    <p>
        Mentions légales<br/> Pas d'escompte pour paiement anticipé.<br/> Conformément à la loi 92.1142, en cas de retard de paiement, toute somme, y compris l'acompte, non payée à sa date d'éxigibilité produira de plein droit des intérêts de retard équivalents au triple du taux d'intérêt légal de l'année en cours ainsi que le paiement d'une somme forfaitaire de quarante (40) euros due au titre des frais de recouvrement conformément au décret N°2012-1115.
    </p>
</div>
<div id="Coordonnées_Bancaires">
    <p><em>Mode de paiement : virement bancaire (frais bancaires à la charge du client. <br /> SOCIETE D'EPARGNE Ile de France - 19 rue des Artistes - 75001 PARIS<br /> BIC : CEPAFRPP000 <br /> IBAN : FR76 0000 0000 0000 0000 0000 0000 </em></p>
</div>
<footer>
    <?php
    if($good == TRUE){
        echo "<p>Télécharger le fichier au format texte.</p>";
        echo "<form action='php/makeDownload.php'> <input type='hidden' name='name' value='$name'/> <input type='submit' value='Facture'/></form>";
    } else{
        echo "<p>Impossible de générer une facture au format texte.</p>";
    }
    ?>
    <p>Retourner à l'accueil</p>
    <form action="accueil.php"> <input type="submit" value="Accueil" > </form>

</footer>
</body>
</html>