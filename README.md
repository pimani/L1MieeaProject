# L1MieeaProject

# ENG

Subject : First year web application project.

co-workers : Barthélemy Pierre & Blondel Mathieu.

Purpose : Create a functional bookshop web site.

Warning : This web site is not real, any purchase is only fictional. We decline any responsibility.

If you notice a bug or an error, please contact administrators at pierre.barthelemy@etu.univ-rouen.fr or mathieu.blondel@etu.univ-rouen.fr.

# FR

Objet : Projet de première année d'Application Web. 

collaborateurs : Barthélémy Pierre & Blondel Mathieu.

Objectif: Créer un site de librairie fonctionnel.

Attention: Ce site n'est pas réel, tout achat n'est que fiction. Nous déclinons toute responsabilité.

Si vous rencontrez un bug ou une erreur, nous vous prions de contacter les administrateurs. 

pierre.barthelemy@etu.univ-rouen.fr ou mathieu.blondel@etu.univ-rouen.fr.


Gource video:
https://youtu.be/bk-JpwpV9T0
