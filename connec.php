<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Connexion</title>
    <meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="CSS/commun.css?<?php echo filemtime('CSS/commun.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="CSS/connect.css?<?php echo filemtime('CSS/connect.css'); ?>" />
</head>

<body>

<header>
<div id="bandeau">
    <form action="accueil.php"> <input type="submit" value="Accueil" > </form>
    <form action="categorie.php"> <input type="submit" value="Catégorie" >
        <div class="sous">
            <a href="categorie.php#Romans_&_Fictions">Romans & Fictions</a>
            <a href="categorie.php#Sciences_Humaines">Sciences Humaines</a>
            <a href="categorie.php#Lettres">Lettres</a>
            <a href="categorie.php#Loisirs_&_Vie_Pratique">Loisirs & Vie Pratique</a>
        </div> </form>
    <form action="contact.php"> <input type="submit" value="Contact" > </form>
    <form action="404.php"><input type="submit" value="Forum" > </form>
    <form action="panier.php"><input type="submit" value="Panier" /> </form>
    <form action="compte.php"> <input type="submit" value="Mon Compte" >
        <div class="sous">
            <?php if(isset($_SESSION['id']) && $_SESSION['id'] != ""){
                echo '<a href="php/disconect.php">Se déconnecter</a>';
            }
            else {
                echo '<a href="connec.php">Se connecter</a>';
                echo '<a href="inscription.php">S\'inscrire</a>';
            }?>
        </div> </form>
    <form action="recherche.php" method="get">
        <input name="recherche" type="text" placeholder="Recherche"/> </form>
</div>
</header>

<div id="shop">
    <div id="header">
        <img src="images/titre.png" alt="titre" >
    </div>
    <p>Se connecter</p>
    <div id="connexion">
<?php

if (!isset($_SESSION['id']) || $_SESSION['id'] == "") {
    echo '<form method="post" action="php/connexion.php">';
    echo '<img src="images/64px/profle.png" alt="Url du site"><br/>';
    echo '<input type="text" name="pseudo" id="pseudo" placeholder="Pseudonyme"/><br/>';
    echo '<input type="password" name="mdp" id="mdp" placeholder="Mot de Passe"/><br/>';
    echo '<input type="hidden" name="validate" id="validate" value="ok"/>';
    echo '<input type="submit" value="Connexion" />';
    echo "</form>";
    if(isset($_GET['reussite'])){
        echo "<p>".$_GET['reussite']."</p>";
    }
}
else{
    echo "<p>Vous êtes connecté(e) en temps que: ".$_SESSION['id']."</p>";
}
?>
    </div>
</div><footer>
    <div id="contact">
        <a href="accueil.php">
            <img src="images/titlefooter.png" alt="Url du site"></a>
        <a href="https://www.facebook.com">
            <img src="images/facebook.png" alt="Url facebook"></a>
        <a href="https://www.twitter.com">
            <img src="images/twitter.png" alt="Url twitter"></a>
        <a href="https://www.youtube.com">
            <img src="images/youtube.png" alt="Url youtube"></a>
        <a href="https://www.linkedin.com">
            <img src="images/linkledin.png" alt="Url linkledin"></a>
        <a href="https://plus.google.com">
            <img src="images/google+.png" alt="Url google+"></a>
        <img src="images/rss.png" alt="Url rss">
        <p><a href="mailto:pierre.barthelemy@etu.univ-rouen.fr,mathieu.blondel@etu.univ-rouen.fr?subject=Contact%20administrateurs%20du%20site%20&body=Bonjour,%20je%20souhaite%20vous%20contacter%20au%20sujet%20de">
                Nous contacter</a> | Téléphone : 02.35.92.34.75 | 76801 Saint-Étienne-du-Rouvray<br /><br />
            À propos du site | CGU & Politique de confidentialité |
            <a href="admin.php">Administration du site</a>
        </p>
    </div>
</footer>

</body>

</html>
