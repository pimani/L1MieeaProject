<?php
session_start();
if (isset($_POST['link'])) {
    $from = $_POST['link'] && $_POST['link'] != "" ? $_POST['link'] : "accueil.php";
}
if(isset($from) && isset($_SESSION['id']) && $_SESSION['id'] != ""){
    header('Location: ../'.$from.'');
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Inscription</title>
        <meta charset="utf-8"/>
        <link type="text/css" rel="stylesheet" href="CSS/commun.css?<?php echo filemtime('CSS/commun.css'); ?>" />
        <link type="text/css" rel="stylesheet" href="CSS/inscription.css?<?php echo filemtime('CSS/inscription.css'); ?>" />
    </head>

    <body>
        <header>
        <div id="bandeau">
            <form action="accueil.php"> <input type="submit" value="Accueil" > </form>
            <form action="categorie.php"> <input type="submit" value="Catégorie" >
                <div class="sous">
                    <a href="categorie.php#Romans_&_Fictions">Romans & Fictions</a>
                    <a href="categorie.php#Sciences_Humaines">Sciences Humaines</a>
                    <a href="categorie.php#Lettres">Lettres</a>
                    <a href="categorie.php#Loisirs_&_Vie_Pratique">Loisirs & Vie Pratique</a>
                </div> </form>
            <form action="contact.php"> <input type="submit" value="Contact" > </form>
            <form action="404.php"><input type="submit" value="Forum" > </form>
            <form action="panier.php"><input type="submit" value="Panier" /> </form>
            <form action="compte.php"> <input type="submit" value="Mon Compte" >
                <div class="sous">
                    <?php if(isset($_SESSION['id']) && $_SESSION['id'] != ""){
                        echo '<a href="php/disconect.php">Se déconnecter</a>';
                    }
                    else {
                        echo '<a href="connec.php">Se connecter</a>';
                        echo '<a href="inscription.php">S\'inscrire</a>';
                    }?>
                </div> </form>
            <form action="recherche.php" method="get">
                <input name="recherche" type="text" placeholder="Recherche"/> </form>
        </div>
        </header>

        <div id="shop">
            <div id="header">
                <img src="images/titre.png" alt="titre">
            </div>
            <form method="post" style="margin: 2%" action="php/inscripton.php">
                <fieldset><legend>Inscription</legend>
                    <table>
                        <tr>
                            <td>
                                <input type="text" id="nom" name="nom" tabindex="1" value="<?php if(isset($_GET['nom'])) echo $_GET['nom']; ?>" placeholder="Nom"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" id="prenom" name="prenom" tabindex="2" value="<?php if(isset($_GET['prenom'])) echo $_GET['prenom']; ?>" placeholder="Prénom"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" id="pseudo" name="pseudo" tabindex="3" value="<?php if(isset($_GET['pseudo'])) echo $_GET['pseudo']; ?>" placeholder="Pseudonyme"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="password" id="mdp" name="mdp" tabindex="4" value="<?php if(isset($_GET['mdp'])) echo $_GET['mdp']; ?>" placeholder="Mot de Passe"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="email" id="email" name="email" tabindex="5" value="<?php if(isset($_GET['email'])) echo $_GET['email']; ?>" placeholder="E-mail"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" id="adresse" name="adresse" tabindex="6" value="<?php if(isset($_GET['adresse'])) echo $_GET['adresse']; ?>" placeholder="Adresse"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <div style="text-align:center; margin-top: 25px;">
                    <input type="submit" name="envoi" value="S'inscrire" />
                    <input type="reset" name="reset" value="Réinitialiser" />
                </div>
            </form>
            <?php
                if(isset($_GET['reussite'])){
                    echo "<p>".$_GET['reussite']."</p>";
                }
            ?>

        </div>
        <div id="contact">
            <a href="accueil.php">
                <img src="images/titlefooter.png" alt="Url du site"></a>
            <a href="https://www.facebook.com">
                <img src="images/facebook.png" alt="Url facebook"></a>
            <a href="https://www.twitter.com">
                <img src="images/twitter.png" alt="Url twitter"></a>
            <a href="https://www.youtube.com">
                <img src="images/youtube.png" alt="Url youtube"></a>
            <a href="https://www.linkedin.com">
                <img src="images/linkledin.png" alt="Url linkledin"></a>
            <a href="https://plus.google.com">
                <img src="images/google+.png" alt="Url google+"></a>
            <img src="images/rss.png" alt="Url rss">
            <p><a href="mailto:pierre.barthelemy@etu.univ-rouen.fr,mathieu.blondel@etu.univ-rouen.fr?subject=Contact%20administrateurs%20du%20site%20&body=Bonjour,%20je%20souhaite%20vous%20contacter%20au%20sujet%20de">
                    Nous contacter</a> | Téléphone : 02.35.92.34.75 | 76801 Saint-Étienne-du-Rouvray<br /><br />
                À propos du site | CGU & Politique de confidentialité |
                <a href="admin.php">Administration du site</a>
            </p>
        </div>
    </body>
</html>
