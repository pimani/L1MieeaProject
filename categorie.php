<?php
session_start();
$ini_array = parse_ini_file("php/conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Catégorie</title>
    <meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="CSS/commun.css?<?php echo filemtime('CSS/commun.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="CSS/categorie.css?<?php echo filemtime('CSS/categorie.css'); ?>" />
</head>

<body>

<header>
<div id="bandeau">
    <form action="accueil.php"> <input type="submit" value="Accueil" > </form>
    <form action="categorie.php"> <input type="submit" value="Catégorie" >
        <div class="sous">
            <a href="categorie.php#Romans_&_Fictions">Romans & Fictions</a>
            <a href="categorie.php#Sciences_Humaines">Sciences Humaines</a>
            <a href="categorie.php#Lettres">Lettres</a>
            <a href="categorie.php#Loisirs_&_Vie_Pratique">Loisirs & Vie Pratique</a>
        </div> </form>
    <form action="contact.php"> <input type="submit" value="Contact" > </form>
    <form action="404.php"><input type="submit" value="Forum" > </form>
    <form action="panier.php"><input type="submit" value="Panier" /> </form>
    <form action="compte.php"> <input type="submit" value="Mon Compte" >
        <div class="sous">
            <?php if(isset($_SESSION['id']) && $_SESSION['id'] != ""){
                echo '<a href="php/disconect.php">Se déconnecter</a>';
            }
            else {
                echo '<a href="connec.php">Se connecter</a>';
                echo '<a href="inscription.php">S\'inscrire</a>';
            }?>
        </div> </form>
    <form action="recherche.php" method="get">
        <input name="recherche" type="text" placeholder="Recherche"/> </form>
</div>
</header>

<div id="shop">
    <div id="header">
        <img src="images/titre.png" alt="titre">
    </div>
    <?php
    if(!$connexion){
        echo "<p>Base de données inaccessible</p>";
    }
    ?>
    <div class="Catégorie" id="Romans_&_Fictions">
        <h3> Romans & Fictions: </h3>
        <table>
        <?php
            $request = "SELECT * FROM produit WHERE categorie='Romans & Fictions'";
            $request = mysqli_query($connexion, $request);
            if(!$request){
                echo "<tr><td>Erreur base de donnée</td></tr>";
            }
            if ($request->num_rows > 0){
                while($row = $request->fetch_assoc()){
                    echo "<tr><td><a href='produit.php?produit=".$row['id']."'>".$row['book']."</a></td></tr>";
                }
            }

        ?>
        </table>
    </div>
    <div class="Catégorie" id="Sciences_Humaines">
        <h3>Sciences Humaines</h3>
        <table>
            <?php
            $request = "SELECT * FROM produit WHERE categorie='Sciences Humaines'";
            $request = mysqli_query($connexion, $request);
            if(!$request){
                echo "<tr><td>Erreur base de donnée</td></tr>";
            }
            if ($request->num_rows > 0){
                while($row = $request->fetch_assoc()){
                    echo "<tr><td><a href='produit.php?produit=".$row['id']."'>".$row['book']."</a></td></tr>";
                }
            }

            ?>
        </table>
    </div>
    <div class="Catégorie" id="Lettres">
        <h3>Lettres</h3>
        <table>
            <?php
            $request = "SELECT * FROM produit WHERE categorie='Lettres'";
            $request = mysqli_query($connexion, $request);
            if(!$request){
                echo "<tr><td>Erreur base de donnée</td></tr>";
            }
            if ($request->num_rows > 0){
                while($row = $request->fetch_assoc()){
                    echo "<tr><td><a href='produit.php?produit=".$row['id']."'>".$row['book']."</a></td></tr>";
                }
            }

            ?>
        </table>
    </div>
    <div class="Catégorie" id="Loisirs_&_Vie_Pratique">
        <h3>Loisirs & Vie Pratique</h3>
        <table>
            <?php
            $request = "SELECT * FROM produit WHERE categorie='Loisirs & Vie Pratique'";
            $request = mysqli_query($connexion, $request);
            if(!$request){
                echo "<tr><td>Erreur base de donnée</td></tr>";
            }
            if ($request->num_rows > 0){
                while($row = $request->fetch_assoc()){
                    echo "<tr><td><a href='produit.php?produit=".$row['id']."'>".$row['book']."</a></td></tr>";
                }
            }

            ?>
        </table>
    </div>
</div>
<footer>
    <div id="contact">
        <a href="accueil.php">
            <img src="images/titlefooter.png" alt="Url du site"></a>
        <a href="https://www.facebook.com">
            <img src="images/facebook.png" alt="Url facebook"></a>
        <a href="https://www.twitter.com">
            <img src="images/twitter.png" alt="Url twitter"></a>
        <a href="https://www.youtube.com">
            <img src="images/youtube.png" alt="Url youtube"></a>
        <a href="https://www.linkedin.com">
            <img src="images/linkledin.png" alt="Url linkledin"></a>
        <a href="https://plus.google.com">
            <img src="images/google+.png" alt="Url google+"></a>
        <img src="images/rss.png" alt="Url rss">
        <p><a href="mailto:pierre.barthelemy@etu.univ-rouen.fr,mathieu.blondel@etu.univ-rouen.fr?subject=Contact%20administrateurs%20du%20site%20&body=Bonjour,%20je%20souhaite%20vous%20contacter%20au%20sujet%20de">
                Nous contacter</a> | Téléphone : 02.35.92.34.75 | 76801 Saint-Étienne-du-Rouvray<br /><br />
            À propos du site | CGU & Politique de confidentialité |
            <a href="admin.php">Administration du site</a>
        </p>
    </div>
</footer>

</body>

</html>