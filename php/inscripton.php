<?php
// Check if the user login is in the data-base and return a error message if needed if add user in the database

$ini_array = parse_ini_file("conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");
$from = $_POST['link'] && $_POST['link'] != "" ? $_POST['link'] : "inscription.php";


function accessData($res, $co, $fr){
    // Exécute la requête sur la base de donnée et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if(!$res){
        mysqli_close($co);
        header("Location: ../".$fr."?réussite=Erreur_base_de_données_requete");
        exit();
    }
    return $res;
}


if (!(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['email']) && isset($_POST['mdp']) && isset($_POST['adresse']) && isset($_POST['pseudo'])
    && $_POST['nom'] != "" && $_POST['prenom'] != "" && $_POST['email'] != "" && $_POST['mdp'] != "" && $_POST['adresse'] != "" && $_POST['pseudo'] != "")){
    // Vérifie si les données principal sont remplis et définis pour savoir si l'utilisateur à donner les informations
    // Minimals pour l'ajout dans la base user
    mysqli_close($connexion);
    header('Location: ../'.$from.'?réussite=Donnée(s)_obligatoire(s)_non_remplie(s)(s).');
    exit();
}


if(!$connexion){
    // Vérifie que la connexion à la base de données à fonctionner
    mysqli_close($connexion);
    header('Location: ../'.$from.'?réussite=Base_de_donne_inaccessible');
    exit();
}

$request = "select * from utilisateur where id='".$_POST['pseudo']."'";
$request = accessData($request, $connexion, $from);

if(mysqli_num_rows($request) != 0 ){
    // Vérifie qu'il n'existe pas déjà un utilisateur avec le même identifiant
    mysqli_close($connexion);
    header('Location: ../'.$from.'?réussite=Utilisateur_déjà_enregistre');
    exit();

}

$request = "insert into utilisateur values('".$_POST['pseudo']."', '".$_POST['nom']."', '".$_POST['prenom']."', FALSE, '".$_POST['mdp']."', '".$_POST['email']."', '".$_POST['adresse']."')";
accessData($request, $connexion, $from);

mysqli_close($connexion);
header('Location: ../'.$from.'?réussite=Succès');
exit();
