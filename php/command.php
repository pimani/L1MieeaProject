<?php
session_start();
//check the id of the user and add a product in his command
$ini_array = parse_ini_file("conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$from = $_POST['link'] && $_POST['link'] != "" ? $_POST['link'] : "accueil.php";

function accessData($res, $co, $fr){
    // Exécute la requête sur la base de donnée et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if(!$res){
        mysqli_close($co);
        header("Location: ../".$fr."?réussite=Erreur_base_de_donnée_requête");
        exit();
    }
    return $res;
}

if($_SESSION['id'] == ""){
    header("Location: ../".$from."?réussite=Veuillez_vous_connecter");
    exit();
}

if(!isset($_POST['id']) || !isset($_POST['nb']) || $_POST['id'] == "" || $_POST['nb'] == "" ||
    !isset($_POST['price']) || $_POST['price'] == ""){
    // Vérifie si les paramètres sont définis et si ils ne sont pas vides
    // pour s'avoir si l'utilisateur à donner un produit
    header("Location: ../".$from."?réussite=Veuillez_préciser_un_produit");
    exit();
}

$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");

if(!$connexion){
    // Vérifie que la connexion à la base c'est bien passer
    mysqli_close($connexion);
    header("Location: $from?réussite=Base_de_donnée_inaccessible");
    exit();
}

$request = "SELECT COUNT(*) as total from utilisateur where id='".$_SESSION['id']."'";
$request = accessData($request, $connexion, $from);
$request = mysqli_fetch_assoc($request);
$request = $request['total'];

if($request == 0){
    // Vérifie si l'utilisateur est dans la base de données
    mysqli_close($connexion);
    header("Location: ../".$from."?reussite=Iddentifiant_incorrect");
    exit();
}

$request = "SELECT COUNT(*) as total from produit where id='".$_POST['id']."'";
$request = accessData($request, $connexion, $from);
$request = mysqli_fetch_assoc($request);
$request = $request['total'];

if($request == 0){
    // Vérifie si le produit est dans la base de données
    mysqli_close($connexion);
    header("Location: ../".$from."?réussite=Produit_n'existe_pas");
    exit();
}

$request = "UPDATE produit SET stock = stock - ".$_POST['nb']." WHERE id='".$_POST['id']."'";
$request = accessData($request, $connexion, $from);

$request = "INSERT INTO command(book, nb, buyer, price) VALUES(".$_POST['id'].",".$_POST['nb'].", '".$_SESSION['id']."', ".($_POST['nb'] * $_POST['price']).")";
$request = accessData($request, $connexion, $from);
mysqli_close($connexion);
header("Location: ../".$from."?Succée");

exit();