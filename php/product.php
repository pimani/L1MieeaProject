<?php
session_start();
//Check if the user is a admin and if the data is correct next add product to database

function file_upload(){
    $imagePath = "../images/";
    $targetFile = $imagePath . basename($_FILES["Pic"]["name"]);
    $fileType = pathinfo($targetFile, PATHINFO_EXTENSION);
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check == false) {
            return "File is not an image.";
        }
    }
    // Check if file already exists
    if (file_exists($targetFile)) {
        return "Sorry, file already exists.";
    }
    // Check file size
    if (isset($_FILES["fileToUpload"]) && $_FILES["fileToUpload"]["size"] > 500000) {
        return "Sorry, your file is too large.";
    }
    // Allow certain file formats
    if($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg"
        && $fileType != "gif" && $fileType != "svg" ) {
        return $targetFile;
    }
    // if everything is ok, try to upload file
    if (move_uploaded_file($_FILES["Pic"]["tmp_name"], $targetFile)) {
        return "".$_FILES["Pic"]["name"]."";
    } else {
        return "Sorry, there was an error uploading your file.".$_FILES['Pic']['tmp_name']." ".$targetFile."";
    }

}

$ini_array = parse_ini_file("conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$from = $_POST['link'] && $_POST['link'] != "" ? $_POST['link'] : "admin.php";

function accessData($res, $co, $fr){
    // Exécute la requête sur la base de donnée et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if(!$res){
        mysqli_close($co);
        header("Location: ../".$fr."?réussite=Erreur_base_de_donnée_requête");
        exit();
    }
    return $res;
}


if($_SESSION['id'] != "admin"){
    // Vérifie si l'utilisateur est bien un administrateur
    header("Location: ../".$from."?réussite=Vous_n'êtes_pas_administrateur");
    exit();
}

if($_POST['Nom'] == "" || $_POST['Auteur'] == "" || $_POST['Stock'] == "" ||  $_POST['Prix'] == "" || $_POST['Categorie'] == ""){
    // Vérifie si l'administrateur à bien envoyer toute les données nécessaires à l'ajout d'un produit
    header("Location: ../".$from."?réussite=Données_non_fournies");
    exit();
}

$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");
if(!$connexion){
    // Vérifie que la connexion à la base s'est bien passé
    mysqli_close($connexion);
    header("Location: ../".$from."?réussite=Base_de_données_inaccessible");
    exit();
}

$request = "insert into produit(book, writer, categorie, stock, price, description, img) value('".$_POST['Nom']."', '".$_POST['Auteur']."', '".$_POST['Categorie']."', ".$_POST['Stock'].", ".$_POST['Prix'].", '".$_POST['Description']."', 'images/".$_FILES['Pic']['name']."')";
var_dump($request);

accessData($request, $connexion, $from);

$file = file_upload();
if($file == basename($_FILES['Pic']['name']) ){
    mysqli_close($connexion);
    header("Location: ../".$from."?réussite=Succès");
    exit();
}
mysqli_close($connexion);
header("Location: ../".$from."?réussite=".$file."");
exit();
