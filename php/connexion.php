<?php
session_start();
// Check if the user login is in the data-base and return a error message if needed if not create session

$ini_array = parse_ini_file("conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");
$from = $_POST['link'] && $_POST['link'] != "" ? $_POST['link'] : "connec.php";


function accessData($res, $co, $fr){
    // Exécute la requête sur la base de données et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if(!$res){
        mysqli_close($co);
        header("Location: ../".$fr."?reussite=Erreur base de données requête");
        exit();
    }
    return $res;
}

if (!( isset($_POST['pseudo']) && isset($_POST['mdp']) && $_POST['pseudo'] != "" && $_POST['mdp'] != "" )) {
    // Vérifie si les variables sont bien existantes et si elles ne sont pas vide affin de savoir si l'utilisateur à envoyer
    // quelque chose
    mysqli_close($connexion);
    header('Location: ../'.$from.'?reussite=Donner vos identifiants');
    exit();
}

if (!$connexion) {
    // Vérifie la connexion à la base de donnée
    mysqli_close($connexion);
    header('Location: ../'.$from.'?reussite=Erreur_base_de_données');
    exit();
}

$request = "select * FROM utilisateur where id='".$_POST['pseudo']."'";
$request = accessData($request, $connexion, $from);

if(mysqli_num_rows($request) != 1) {
    // Vérifie l'existance de l'utilisateur dans la base de donnée
    mysqli_close($connexion);
    header('Location: ../'.$from.'?reussite=Utilisateur_pas');
    exit();
}

$resultat = mysqli_fetch_array($request);
$resultat = $resultat["mdp"];


if ($resultat == $_POST['mdp']){
    mysqli_close($connexion);
    $_SESSION['id'] = $_POST['pseudo'];
    header('Location: ../'.$from.'?reussite=Succès');
    exit();
}

else {
    header('Location: ../'.$from.'?reussite=Mauvais_mdp');
    exit();
}