<?php
session_start();

if(!isset($_SESSION['id']) || $_SESSION['id'] == ""){
    header('Location: ../404.php');
    exit();
}

if(!(isset($_POST['oldMdp']) && isset($_POST['newMdp1']) && isset($_POST['newMdp2'])) || $_POST['oldMdp'] == "" ||
    $_POST['newMdp1'] == "" || $_POST['newMdp2'] == "" ){
    header('Location: ../404.php?Succès=Veuillez_donner_toutes_les_informations');
    exit();
}

$ini_array = parse_ini_file("conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");
$from = $_POST['link'] && $_POST['link'] != "" ? $_POST['link'] : "compte.php";


function accessData($res, $co, $fr){
    // Exécute la requête sur la base de donnée et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if(!$res){
        mysqli_close($co);
        header("Location: ../".$fr."?réussite=Erreur_base_de_données_requête");
        exit();
    }
    return $res;
}

$request = "SELECT mdp AS total from utilisateur where id='".$_SESSION['id']."'";
$request = accessData($request, $connexion, $from);
$request = mysqli_fetch_assoc($request);
$request = $request['total'];

if($_POST['oldMdp'] != $request){
    mysqli_close($connexion);
    header('Location: ../404.php?Succès=Mauvais_mdp');
    exit();
}

if($_POST['newMdp1'] != $_POST['newMdp2']){
    mysqli_close($connexion);
    header('Location: ../404.php?Succès=Les_mdp_corresponde_pas');
    exit();
}

$request = "UPDATE utilisateur SET mdp ='".$_POST['newMdp1']."' WHERE id='".$_SESSION['id']."'";
var_dump($request);

accessData($request, $connexion, $from);

mysqli_close($connexion);
header('Location: ../compte.php?Succès=Succès');
exit();