<?php

session_start();

$_SESSION['captcha'] = mt_rand(1000,9999);
$img = imagecreate(63, 33);
$font = '../fonts/OldLondon.ttf';

$bg = imagecolorallocate($img, 253, 246, 230);
$textcolor = imagecolorallocate($img, 48, 48, 48);

imagettftext($img, 23, 0, 6, 26, $textcolor, $font, $_SESSION['captcha']);

header('Content-type:image/jpeg');
imagejpeg($img);
imagedestroy($img);