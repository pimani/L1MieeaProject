<?php
session_start();

$from = $_POST['link'] && $_POST['link'] != "" ? $_POST['link'] : "admin.php";


function accessData($res, $co, $fr){
    // Exécute la requête sur la base de donnée et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if(!$res){
        mysqli_close($co);
        header("Location: ../".$fr."?réussite=Erreur_base_de_données_requête");
        exit();
    }
    return $res;
}

if(!isset($_POST['nb1']) && !isset($_POST['select1']) && $_POST['select1'] != "" && $_POST['nb1'] != "" &&
    !isset($_POST['nb2']) && !isset($_POST['select2']) && $_POST['select2'] != "" && $_POST['nb2'] != "" &&
    !isset($_POST['nb3']) && !isset($_POST['select3']) && $_POST['select3'] != "" && $_POST['nb3'] != "" &&
    !isset($_POST['nb4']) && !isset($_POST['select4']) && $_POST['select4'] != "" && $_POST['nb4'] != "" &&
    !isset($_POST['nb5']) && !isset($_POST['select5']) && $_POST['select5'] != "" && $_POST['nb5'] != ""){
    header('Location: ../'.$from.'?réussite=Données_obligatoires_non_remplies');
    exit();
}

if(!isset($_SESSION['id']) || $_SESSION['id'] == ""){
    header('Location: ../'.$from.'?réussite=Mauvaise_session');
    exit();
}

$ini_array = parse_ini_file("conf.ini");
$id = $ini_array["id"];
$mpd = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mpd, $table);
mysqli_set_charset($connexion, "utf8");

if(!$connexion){
    // Vérifie que la connexion à la base de données a fonctionné
    mysqli_close($connexion);
    header('Location: ../'.$from.'?réussite=Base_de_données_inaccessible');
    exit();
}

if($_POST["select1"] != "Aucun"){
    $request = "UPDATE produit SET stock = stock + ".$_POST['nb1']." WHERE id='".$_POST['select1']."'";
    $request = accessData($request, $connexion, $from);
}

if($_POST["select2"] != "Aucun"){
    $request = "UPDATE produit SET stock = stock + ".$_POST['nb2']." WHERE id='".$_POST['select2']."'";
    $request = accessData($request, $connexion, $from);
}

if($_POST["select3"] != "Aucun"){
    $request = "UPDATE produit SET stock = stock + ".$_POST['nb3']." WHERE id='".$_POST['select3']."'";
    $request = accessData($request, $connexion, $from);
}

if($_POST["select4"] != "Aucun"){
    $request = "UPDATE produit SET stock = stock + ".$_POST['nb4']." WHERE id='".$_POST['select4']."'";
    $request = accessData($request, $connexion, $from);
}

if($_POST["select5"] != "Aucun"){
    $request = "UPDATE produit SET stock = stock + ".$_POST['nb5']." WHERE id='".$_POST['select5']."'";
    $request = accessData($request, $connexion, $from);
}

mysqli_close($connexion);
header('Location: ../'.$from.'?réussite=Succée');
exit();