<?php
session_start();

$ini_array = parse_ini_file("php/conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");

function accessData($res, $co){
    // Exécute la requête sur la base de données et renvoie vers la page d'origine (from) avec un message d'erreur dans
    // réussite via get en cas d'erreur
    $res = mysqli_query($co, $res);
    if(!$res){
        return NULL;
    }
    return $res;
}

if(isset($_SESSION['id']) && $_SESSION['id'] != "") {
    $request = "SELECT * FROM command WHERE buyer='".$_SESSION['id']."'";
    $request = accessData($request, $connexion);
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Panier</title>
    <meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="CSS/commun.css?<?php echo filemtime('CSS/commun.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="CSS/panier.css?<?php echo filemtime('CSS/panier.css'); ?>" />
</head>

<body>

<header>
<div id="bandeau">
    <form action="accueil.php"> <input type="submit" value="Accueil" > </form>
    <form action="categorie.php"> <input type="submit" value="Catégorie" >
        <div class="sous">
            <a href="categorie.php#Romans_&_Fictions">Romans & Fictions</a>
            <a href="categorie.php#Sciences_Humaines">Sciences Humaines</a>
            <a href="categorie.php#Lettres">Lettres</a>
            <a href="categorie.php#Loisirs_&_Vie_Pratique">Loisirs & Vie Pratique</a>
        </div> </form>
    <form action="contact.php"> <input type="submit" value="Contact" > </form>
    <form action="404.php"><input type="submit" value="Forum" > </form>
    <form action="panier.php"><input type="submit" value="Panier" /> </form>
    <form action="compte.php"> <input type="submit" value="Mon Compte" >
        <div class="sous">
            <?php if(isset($_SESSION['id']) && $_SESSION['id'] != ""){
                echo '<a href="php/disconect.php">Se déconnecter</a>';
            }
            else {
                echo '<a href="connec.php">Se connecter</a>';
                echo '<a href="inscription.php">S\'inscrire</a>';
            }?>
        </div> </form>
    <form action="recherche.php" method="get">
        <input name="recherche" type="text" placeholder="Recherche"/> </form>
</div>
</header>

<div id="shop">
    <div id="header">
        <img src="images/titre.png" alt="titre">
    </div>
    <h2>Mon Panier</h2>
<?php
    echo "<div id='panier'>";
    if(!isset($_SESSION['id']) || $_SESSION['id'] == ""){
        echo "<p>Vous n'êtes pas connecté.</p>";
    }
    elseif (isset($request)){
        if(mysqli_num_rows($request) == 0){
            echo "<p>Votre panier est vide.</p>";
        }
        else {

            echo "<table>";
            echo "<tr><th>Livre :</th><th>Nombre</th><th>Prix</th></tr>";
            for($i = 0; $i < mysqli_num_rows($request); $i++){
                $data = mysqli_fetch_array($request);

                $livre = "SELECT * FROM produit WHERE id='".$data['book']."'";
                $livre = accessData($livre, $connexion);
                $livre = mysqli_fetch_array($livre);
                if (isset($totalNb) && isset($totalPrice)) {
                    $totalNb = $totalNb + $data['nb'];
                    $totalPrice = $totalPrice + $livre['price']*$data['nb'];
                } else {
                    $totalNb = $data['nb'];
                    $totalPrice = $livre['price']*$data['nb'];
                }

                echo "<tr><td>".$livre['book']." :</td><td>".$data['nb']."</td>
                        <td>".$livre['price']."€</td></tr>";
            }
            if (isset($totalNb) && isset($totalPrice)) {
                echo "<tr><td>Total :</td>
                        <td>".$totalNb."</td>
                            <td>".$totalPrice."€</td></tr>";
            }
            echo "</table>";
        }
    }
    echo "</div>";
?>
</div>

<footer>
    <div id="contact">
        <a href="accueil.php">
            <img src="images/titlefooter.png" alt="Url du site"></a>
        <a href="https://www.facebook.com">
            <img src="images/facebook.png" alt="Url facebook"></a>
        <a href="https://www.twitter.com">
            <img src="images/twitter.png" alt="Url twitter"></a>
        <a href="https://www.youtube.com">
            <img src="images/youtube.png" alt="Url youtube"></a>
        <a href="https://www.linkedin.com">
            <img src="images/linkledin.png" alt="Url linkledin"></a>
        <a href="https://plus.google.com">
            <img src="images/google+.png" alt="Url google+"></a>
        <img src="images/rss.png" alt="Url rss">
        <p><a href="mailto:pierre.barthelemy@etu.univ-rouen.fr,mathieu.blondel@etu.univ-rouen.fr?subject=Contact%20administrateurs%20du%20site%20&body=Bonjour,%20je%20souhaite%20vous%20contacter%20au%20sujet%20de">
                Nous contacter</a> | Téléphone : 02.35.92.34.75 | 76801 Saint-Étienne-du-Rouvray<br /><br />
            À propos du site | CGU & Politique de confidentialité |
            <a href="admin.php">Administration du site</a>
        </p>
    </div>
</footer>
</body>
</html>
