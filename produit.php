<?php
session_start();

$ini_array = parse_ini_file("php/conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");

if($_GET['produit'] != "") {
    $request = "SELECT * FROM produit WHERE id=(".$_GET['produit'].")";
    $request = mysqli_query($connexion, $request);
    if(mysqli_num_rows($request) != 1) {
        header('Location: 404.php');
        exit();
    }
    $data = mysqli_fetch_array($request);
}

else{
    header('Location: index.php');
    exit();
}


if (isset($_SESSION['id']) && $_SESSION['id'] != ""){
    $request = "SELECT SUM(price) AS total FROM command WHERE buyer='".$_SESSION['id']."'";
    $request = mysqli_query($connexion, $request);
    if($request){
        $request = mysqli_fetch_array($request);
        $totalPrice = $request['total'];
    }
    else{
        $totalPrice = "Erreur base de données";
    }
    $request = "SELECT SUM(nb) AS total FROM command WHERE buyer='".$_SESSION['id']."'";
    $request = mysqli_query($connexion, $request);
    if($request){
        $resultat = mysqli_fetch_array($request);
        $totalNb = $resultat['total'];
    }
    else{
        $totalNb = "Erreur base de données";
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title><?php echo $data['book']; ?></title>
    <meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="CSS/commun.css?<?php echo filemtime('CSS/commun.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="CSS/produit.css?<?php echo filemtime('CSS/produit.css'); ?>" />
</head>

<body>
<header>
<div id="bandeau">
    <form action="accueil.php"> <input type="submit" value="Accueil" > </form>
    <form action="categorie.php"> <input type="submit" value="Catégorie" >
        <div class="sous">
            <a href="categorie.php#Romans_&_Fictions">Romans & Fictions</a>
            <a href="categorie.php#Sciences_Humaines">Sciences Humaines</a>
            <a href="categorie.php#Lettres">Lettres</a>
            <a href="categorie.php#Loisirs_&_Vie_Pratique">Loisirs & Vie Pratique</a>
        </div> </form>
    <form action="contact.php"> <input type="submit" value="Contact" > </form>
    <form action="404.php"><input type="submit" value="Forum" > </form>
    <form action="panier.php"><input type="submit" value="Panier" /> </form>
    <form action="compte.php"> <input type="submit" value="Mon Compte" >
        <div class="sous">
            <?php if(isset($_SESSION['id']) && $_SESSION['id'] != ""){
                echo '<a href="php/disconect.php">Se déconnecter</a>';
            }
            else {
                echo '<a href="connec.php">Se connecter</a>';
                echo '<a href="inscription.php">S\'inscrire</a>';
            }?>
        </div> </form>
    <form action="recherche.php" method="get">
        <input name="recherche" type="text" placeholder="Recherche"/> </form>
</div>
</header>

<?php
    echo "<div id=\"shop\">
            <div id=\"header\">
                <img src=\"/images/titre.png\" alt=\"titre\">
            </div>";
        echo "<h3>".$data['book']."</h3>";
        echo "<div class='ligne'><img src=".$data['img']." class='pic' alt='Problème de connexion'/></div>";
        echo "<div class='ligne' id='presentation'>";
            echo "<p>Prix: <span id='prix'>".$data['price']." €</span></p>";
            echo "<p>Stock: <span id='EnStock'>".$data['stock']."</span></p>";
            echo "<p>Description: <span id='description'>".$data['description']."<br/></span></p>";
            if(isset($_SESSION['id']) && $_SESSION['id'] != "") {
                echo "<form action='php/command.php' method='post'>";
                echo "<select name='nb'>";
                for ($i = 0; $i <= $data['stock']; $i++) {
                    echo "<option value=$i>$i</option>";
                }
                echo "</select>";
                echo '<input type="hidden" id="price" name="price" value='.$data["price"].'>';
                echo '<input type="hidden" id="id" name="id" value='.$data["id"].'>';
                echo '<input type="submit" name="book" value="Commander" />';
                echo "</form>";
            }
        echo "</div>";
        if(isset($_SESSION['id']) && $_SESSION['id'] != ""){
            echo "<div class='ligne' id='panier'>";
                echo "<img src='https://image.flaticon.com/icons/png/512/9/9776.png' alt='Panier' class='pic'/>";
                if (isset ($totalPrice) && isset($totalNb)) {
                    echo "<p>Valeur: ".$totalPrice."€<br/>Nombre de produit: ".$totalNb."</p>";
                } else {
                    echo "<p>Valeur: 0.00 €<br/>Nombre de produit: 0</p>";
                }
            echo "</div>";
        }?>
        <div id='conseil'>
            <div class="gallery">
                <a target="_blank" href="accueil.php">
                <img src="images/b00.png" alt="sortie" width="300" height="200"></a>
                <div class="desc">Livre bientôt en vente !</div>
            </div>
        </div>
<?php    echo "</div>"; ?>
<footer>
    <div id="contact">
        <a href="accueil.php">
            <img src="images/titlefooter.png" alt="Url du site"></a>
        <a href="https://www.facebook.com">
            <img src="images/facebook.png" alt="Url facebook"></a>
        <a href="https://www.twitter.com">
            <img src="images/twitter.png" alt="Url twitter"></a>
        <a href="https://www.youtube.com">
            <img src="images/youtube.png" alt="Url youtube"></a>
        <a href="https://www.linkedin.com">
            <img src="images/linkledin.png" alt="Url linkledin"></a>
        <a href="https://plus.google.com">
            <img src="images/google+.png" alt="Url google+"></a>
        <img src="images/rss.png" alt="Url rss">
        <p><a href="mailto:pierre.barthelemy@etu.univ-rouen.fr,mathieu.blondel@etu.univ-rouen.fr?subject=Contact%20administrateurs%20du%20site%20&body=Bonjour,%20je%20souhaite%20vous%20contacter%20au%20sujet%20de">
                Nous contacter</a> | Téléphone : 02.35.92.34.75 | 76801 Saint-Étienne-du-Rouvray<br /><br />
            À propos du site | CGU & Politique de confidentialité |
            <a href="admin.php">Administration du site</a>
        </p>
    </div>
</footer>
</body>
</html>
