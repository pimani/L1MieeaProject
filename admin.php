<?php
session_start();

$ini_array = parse_ini_file("php/conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");

function accessData($res, $co){
    // Exécute la requête sur la base de donnée et renvoie vers la page d'origine
    // (from) avec un message d'erreur dans réussite via get en cas d'erreur
    $res = mysqli_query($co, $res);
    if(!$res){
        return NULL;
    }
    return $res;
}

$check = false;
if(isset($_SESSION['id']) && $_SESSION != "" ){
    $request = "SELECT * from utilisateur WHERE id='".$_SESSION['id']."'";
    $request = accessData($request, $connexion);
    if(mysqli_num_rows($request) == 1){
        $resultat = mysqli_fetch_array($request);
        $check = $resultat["admin"];
    }
}

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Administration</title>
        <meta charset="utf-8"/>
        <link type="text/css" rel="stylesheet" href="CSS/admin.css?<?php echo filemtime('CSS/admin.css'); ?>" />
    </head>

    <body>
        <h1>Panel d'administration</h1>
        <?php
        if (!$check) {
            echo '<form name="connexion" id="connexion" method="post" action="php/connexion.php">
            <table id="conec">
                <tr>
                    <td><label for="Pseudo" class="float">Pseudo :</label></td>
                    <td><input type="text" name="pseudo" id="pseudo"
                       value=""/></td>
                </tr>
                <tr>
                    <td><label for="mdp" class="float">Mot de passe:</label></td><td><input type="password" name="mdp" id="mdp"/></td>
                </tr>
                <tr>
                    <td><input type="hidden" name="link" id="link" value="admin.php"/></td>
                </tr>
            </table>
            <div class="center"><input type="submit" value="Connexion" /></div>
            </form>

            <p>
                <a href="connec.php?action=reset">J\'ai oublié mon mot de passe !</a>
            </p>';
        }
        else{
            echo "
                  <div id='menus'>
                    <a href='admin.php?part=main'>Page principale</a>
                    <a href='admin.php?part=produit'>Gestion des produits et stocks</a>
                    <a href='admin.php?part=utilisateur'>Gestion des utilisateurs</a>
                    <a href='admin.php?part=commandes'>Gestions des commandes</a>
                    <a href='accueil.php'>Retour vers le site</a>
                    </div>  
                  <div id='main'>";
            if(!isset($_GET['part']) || ($_GET['part'] == "" || $_GET['part'] == "main")){
                echo "<h2>Page d'administration</h2>";
                $ini_array = parse_ini_file("php/conf.ini");
                $id = $ini_array["id"];
                $mdp = $ini_array["mdp"];
                $table = $ini_array["table"];
                $link = $ini_array["link"];
                $connexion = mysqli_connect($link, $id, $mdp, $table);
                mysqli_set_charset($connexion, "utf8");
                if($connexion){
                    $user = mysqli_query($connexion, "select * from utilisateur");
                    $produit = mysqli_query($connexion, "select * from produit");
                    $dispo = mysqli_query($connexion, "select * from produit where stock > 0");
                    $user = mysqli_num_rows($user);
                    $dispo = mysqli_num_rows($dispo);
                    $produit = mysqli_num_rows($produit);
                    echo "<div class='content'><img src='images/iconeLUser.svg' alt='Utilisateur'/><p>Utilisateur: ".$user."</p></div>";
                    echo "<div class='content'><img src='images/iconeLivre.png' alt='Produit'/><p>Produit: ".$produit."</p></div>";
                    echo "<div class='content'><img src='images/iconeCommande.png' alt='Livre'/><p>Livre disponible: ".$dispo."/".$produit."</p></div>";
                }
                else{
                    echo "<h1 style='color: red;'>Erreur de connexion avec la base de données</h1>";
                }
            }
            if(isset($_GET['part']) && $_GET['part'] == "produit"){
                $ini_array = parse_ini_file("php/conf.ini");
                $id = $ini_array["id"];
                $mdp = $ini_array["mdp"];
                $table = $ini_array["table"];
                $link = $ini_array["link"];
                $connexion = mysqli_connect($link, $id, $mdp, $table);
                mysqli_set_charset($connexion, "utf8");
                echo "<h2>Page de gestion des produits</h2>";
                echo '
                <form name="connexion" id="connexion" method="post" action="php/product.php" enctype="multipart/form-data">
                    <table id="conec">
                        <tr>
                            <td><label for="Nom" class="float">Nom du livre :</label></td>
                            <td><input type="text" name="Nom" id="Nom" value=""/></td>
                        </tr>
                        <tr>
                            <td><label for="Auteur" >Nom de l\'auteur :</label></td><td><input type="text" name="Auteur" id="Auteur"/></td>
                        </tr>
                        <tr>
                            <td><label for="Categorie">Catégorie du livre</label></td>
                            <td>
                                <select name="Categorie" id="Categorie">
                                  <option value="Romans & Fictions">Romans & Fictions</option>
                                  <option value="Sciences Humaines">Sciences Humaines</option>
                                  <option value="Lettres">Lettres</option>
                                  <option value="Loisirs & Vie Pratique">Loisirs & Vie Pratique</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="Stock" >Livre en stock :</label></td><td><input type="text" name="Stock" id="Stock"/></td>
                        </tr>
                        <tr>
                            <td><label for="Prix" >Prix du livre :</label></td><td><input type="text" name="Prix" id="Prix"/></td>
                        </tr>
                        <tr>
                            <td><label for="Pic">Image du livre :</label></td><td><input class="input-file" type="file" name="Pic" id="Pic"/></td>
                        </tr>
                        <tr>
                            <td><label for="Description">Description :</label></td>
                            <td><textarea id="Description" name="Description" tabindex="4" cols="50" rows="8"></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="hidden" name="link" id="link" value="admin.php"/></td>
                        </tr>
                    </table>
                    <div class="center"><input type="submit" value="Validation" /></div>
                </form>';

                if($connexion){
                    $produit = mysqli_query($connexion, "select * from produit");
                    if ($produit->num_rows > 0) {
                        $result = array(array(-1, "Aucun"));
                        while($row = $produit->fetch_assoc()){
                            array_push($result, array($row['id'], $row['book']));
                        }
                    }

                }
                ?>
                <form action="php/reaprovisioner.php" method="post">
                    <table id="choix">
                        <tr>
                            <td><select id="select1" name="select1"><?php foreach($result as $row) {echo "<option value=".$row[0].">".$row[1]."</option>"; }?></select>
                            </td><td><input type="number" min="0" id="nb1" name="nb1" value="0"/></td>
                        </tr>
                        <tr>
                            <td><select id="select2" name="select2"><?php foreach($result as $row) {echo "<option value=".$row[0].">".$row[1]."</option>"; }?></select>
                            </td><td><input type="number" min="0" id="nb2" name="nb2" value="0"/></td>
                        </tr>
                        <tr>
                            <td><select id="select3" name="select3"><?php foreach($result as $row) {echo "<option value=".$row[0].">".$row[1]."</option>"; }?></select>
                            </td><td><input type="number" min="0" id="nb3" name="nb3" value="0"/></td>
                        </tr>
                        <tr>
                            <td><select id="select4" name="select4"><?php foreach($result as $row) {echo "<option value=".$row[0].">".$row[1]."</option>"; }?></select>
                            </td><td><input type="number" min="0" id="nb4" name="nb4" value="0"/></td>
                        </tr>
                        <tr>
                            <td><select id="select5" name="select5"><?php foreach($result as $row) {echo "<option value=".$row[0].">".$row[1]."</option>"; }?></select>
                            </td><td><input type="number" min="0" id="nb5" name="nb5" value="0"/></td>
                        </tr>
                    </table>
                    <div class="center"><input type="submit" value="Validation" /></div>
                </form>
                <?php
                echo "<br/>";
                if($connexion){
                    $produit = mysqli_query($connexion, "select * from produit");
                    if ($produit->num_rows > 0){
                        echo "<table class='show'>";
                        echo "<tr><th>Id</th><th>Nom du livre</th><th>Stock</th><th>Prix</th></tr>";
                        while($row = $produit->fetch_assoc()){
                            echo "<tr><td>".$row['id']."</td><td>".$row['book']."</td>";
                            if ($row['stock'] > 0 && $row['stock'] <= 5){
                                echo "<td style='color:orange;'>".$row['stock']."</td>";
                            }
                            elseif ($row['stock'] == 0){
                                echo "<td style='color:red;'>".$row['stock']."</td>";
                            }
                            else{
                                echo "<td>".$row['stock']."</td>";
                            }
                            echo"<td>".$row['price']." €</td></tr>";
                        }
                        echo "</table>";
                    }
                    else {
                        echo "<h3>Il n'y a aucun livre dans la base de données.</h3>";
                    }
                }
            }
            elseif (isset($_GET['part']) &&  $_GET['part'] == "utilisateur"){
                echo "<h2>Page de gestion des utilisateurs</h2>";
                $ini_array = parse_ini_file("php/conf.ini");
                $id = $ini_array["id"];
                $mdp = $ini_array["mdp"];
                $table = $ini_array["table"];
                $link = $ini_array["link"];
                $connexion = mysqli_connect($link, $id, $mdp, $table);
                mysqli_set_charset($connexion, "utf8");
                if($connexion){
                    $produit = mysqli_query($connexion, "select * from utilisateur");
                    if ($produit->num_rows >= 1){
                        echo "<table class='show'>";
                        echo "<tr><th>Id</th><th>Nom</th><th>Prénom</th><th>Mail</th></tr>";
                        while($row = $produit->fetch_assoc()){
                            echo "<tr><td>".$row['id']."</td><td>".$row['nom']."</td>";
                            echo "<td>".$row['prenom']."</td><td>".$row['email']."</td></tr>";
                        }
                        echo "</table>";
                    }
                    else {
                        echo "<h3>Il n'y a aucun utilisateur.</h3>";
                    }
                }
            }elseif (isset($_GET['part']) &&  $_GET['part'] == "commandes"){
                echo "<h2>Page de gestion des commandes</h2>";
                $ini_array = parse_ini_file("php/conf.ini");
                $id = $ini_array["id"];
                $mdp = $ini_array["mdp"];
                $table = $ini_array["table"];
                $link = $ini_array["link"];
                $connexion = mysqli_connect($link, $id, $mdp, $table);
                mysqli_set_charset($connexion, "utf8");
                if($connexion){
                    $produit = mysqli_query($connexion, "select * from command");
                    if ($produit->num_rows > 0){
                        echo "<table class='show'>";
                        echo "<tr><th>Numéro de commande</th><th>Id du livre</th><th>Prix de la commande</th><th>Acheteur</th></tr>";
                        while($row = $produit->fetch_assoc()){
                            echo "<tr><td>n°".$row['id']."</td><td>".$row['book']."</td>";
                            echo "<td>".$row['price']." €</td><td>".$row['buyer']."</td></tr>";
                        }
                        echo "</table>";
                    }
                    else {
                        echo "<h3>Il n'y a aucune commande.</h3>";
                    }
                }
            }
            echo "</div>";
        }
    ?>
    </body>

</html>
