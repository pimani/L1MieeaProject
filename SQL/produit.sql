create table produit (
	id INT(20) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
  book char(100) NOT NULL,
  writer char(40) NOT NULL,
  categorie char (30) NOT NULL,
  stock int(20) NOT NULL,
	price int(20) NOT NULL,
  description text(500) NOT NULL,
  img char(50) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into  produit(book, writer, categorie, stock, price, description, img) values
('Révolution:1870-71','Jules_claretie', 'Sciences Humaines', 1, 40, 'descrévolution', 'images/Révolution1870-71.jpg');
insert into  produit(book, writer, categorie, stock, price, description, img) values
('l\'Être contre l\'avoir','Francis_Cousin', 'Sciences Humaines', 10, 21, 'descl\'etrecontrel\'avoir',
 'images/l-etre-contre-l-avoir-cote.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
('Commentaires sur l\'extrême radicalité des temps derniers','Francis_Cousin', 'Sciences Humaines', 12, 21,'desccommentaires','images/commentairescote.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('Essais','Michel_Drac', 'Sciences Humaines', 10, 22,'descessais','images/b12.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('Les secrets de la Réserve Fédérale','Eustace_Mullins', 'Sciences Humaines', 26, 22,'descessais','images/b22.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('Rues Barbares','Piero_San_Giorgio', 'Sciences Humaines', 10, 22,'descruesbarbares','images/b32.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('Les cinq stades de l\'effondrement','Dmitry_Orlov', 'Lettres', 48, 21,'desclescinqstades','images/a.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('Retour sur Maïdan','Lucien_Cerise', 'Romans & Fictions', 3, 21,'descretoursurmaidan','images/a1.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('NRBC','Piero_San_Giorgio', 'Sciences Humaines', 33, 26,'descnrbc','images/a2.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('Rues Barbares','Piero_San_Giorgio', 'Sciences Humaines', 10, 22,'descruesbarbares','images/a3.png');
insert into  produit(book, writer, categorie, stock, price, description, img) values
  ('La société de l\'indécence','Stuart_Ewen', 'Loisirs & Vie Pratique', 8, 16,'desclasociétéde','images/a4.png');