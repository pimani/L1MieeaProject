CREATE TABLE utilisateur(
  id CHAR(20) NOT NULL,
  nom CHAR(20) NOT NULL,
  prenom CHAR(20) NOT NULL,
  admin BOOLEAN NOT NULL,
  mdp CHAR(20) NOT NULL,
  email CHAR(20) NOT NULL,
  adresse CHAR(255) NOT NULL,
  PRIMARY KEY(id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
insert into utilisateur(id, nom, prenom, admin, mdp, email, adresse) VALUES
  ('admin', 'lenom', 'leprénom', TRUE, '123456789', 'admin@mail.fr', '29bis ajrofezh');
insert into utilisateur(id, nom, prenom, admin, mdp, email, adresse) VALUES
  ('random', 'random', 'random', FALSE, 'pomme', 'pomme@gmail.com', 'ici' );
