create table command (
	id int(20) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
  book int(20) NOT NULL,
  nb int(20) NOT NULL,
  buyer char(20) NOT NULL,
  price int(20) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;