<?php
session_start();

$ini_array = parse_ini_file("php/conf.ini");
$id = $ini_array["id"];
$mdp = $ini_array["mdp"];
$table = $ini_array["table"];
$link = $ini_array["link"];
$connexion = mysqli_connect($link, $id, $mdp, $table);
mysqli_set_charset($connexion, "utf8");

function accessData($res, $co){
    // Exécute la requête sur la base de donnée et renvois vers la page d'origine (from) avec un message d'érreur dans
    // réussite via get en cas d'érreur
    $res = mysqli_query($co, $res);
    if(!$res){
        $res = "ERREUR_BASE_DE_DONNÉES";
    }
    return $res;
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Accueil</title>
	<meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="CSS/commun.css?<?php echo filemtime('CSS/commun.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="CSS/accueil.css?<?php echo filemtime('CSS/accueil.css'); ?>" />
</head>

<body>
    <header>
        <div id="bandeau">
            <form action="accueil.php"> <input type="submit" value="Accueil"> </form>
            <form action="categorie.php"> <input type="submit" value="Catégorie">
                <div class="sous">
                    <a href="categorie.php#Romans_&_Fictions">Romans & Fictions</a>
                    <a href="categorie.php#Sciences_Humaines">Sciences Humaines</a>
                    <a href="categorie.php#Lettres">Lettres</a>
                    <a href="categorie.php#Loisirs_&_Vie_Pratique">Loisirs & Vie Pratique</a>
                </div> </form>
            <form action="contact.php"> <input type="submit" value="Contact" > </form>
            <form action="404.php"><input type="submit" value="Forum" > </form>
            <form action="panier.php"><input type="submit" value="Panier" /> </form>
            <form action="compte.php"> <input type="submit" value="Mon Compte" >
                <div class="sous">
                    <?php if(isset($_SESSION['id']) && $_SESSION['id'] != ""){
                        echo '<a href="php/disconect.php">Se déconnecter</a>';
                    }
                    else {
                        echo '<a href="connec.php">Se connecter</a>';
                        echo '<a href="inscription.php">S\'inscrire</a>';
                    }?>
                    </div> </form>
            <form action="recherche.php" method="get">
                <input name="recherche" type="text" placeholder="Recherche"/> </form>
        </div>
    </header>

        <div id="shop">

            <div id="header">
                <img src="images/titre.png" alt="titre">
            </div>

            <div id="news">
                <h2> Dernières Parutions</h2>

                <!--/*
                $request = "SELECT * FROM produit ORDER BY id DESC LIMIT 3";
                $request = accessData($request, $connexion);
                if($request == "ERREUR_BASE_DE_DONNE"){
                    echo "<p>$request</p>";
                }
                else{
                    $data1 = $request->fetch_assoc();
                    $data2 = $request->fetch_assoc();
                    $data3 = $request->fetch_assoc();
                }
                < echo$data1['id']; ?>
                < echo $data1['img'] ?>
                < echo$data2['id']; ?>
                < echo $data2['img'] ?>
                < echo$data3['id']; ?>
                < echo $data3['img'] ?>

                -->
                <div class="w3-content">
                    <a href="produit.php?produit=4">
                    <img class="mySlides" src="images/diapo1.jpg" alt="diapo1"></a>
                        <a href="produit.php?produit=5">
                    <img class="mySlides" src="images/diapo2.jpg" alt="diapo2"></a>
                        <a href="produit.php?produit=6">
                    <img class="mySlides" src="images/diapo3.jpg" alt="diapo3"></a>

                    <div class="w3-row-padding">
                        <table><tbody>
                            <tr><td><img class="demo w3-opacity w3-hover-opacity-off" src="images/b11.jpg" alt="switch1" onclick="currentDiv(1)"></td>
                                <td><img class="demo w3-opacity w3-hover-opacity-off" src="images/b21.jpg" alt="switch2" onclick="currentDiv(2)"></td>
                                <td><img class="demo w3-opacity w3-hover-opacity-off" src="images/b31.jpg" alt="switch3" onclick="currentDiv(3)"></td></tr>
                            </tbody></table>
                    </div>
                </div>
            </div>
            <hr>
            <div id="vente">
                <h2>Notre Meilleure Vente</h2>
                    <table><tbody>
                        <tr><td class="image"><a href="produit.php?produit=3">
                                <img src="images/commentairescote.png" alt="Meilleure vente"></a></td>
                        <td class="résumé" rowspan="2">Voilà plus de trente ans que Francis Cousin s'est attaché à produire une critique radicale du spectacle mondialiste de la marchandise autocratique, de sa décadence universelle, de ses manœuvres monétaires intensives et de ses grandes manipulations terroristes étatiques. Ainsi, il en dé-voile ici la généalogie, le développement et la fin en identifiant la dialectique historique des longues durées par laquelle a pu finir par se réaliser l’économie politique de la tyrannie démocratique du mouvement de l’argent. Il est, entre autres ouvrages, l’auteur de <span class="fort">L’être contre l’Avoir</span>…<br><br>
                            En prolongement de la pensée critique des présocratiques, de Hegel et de Marx et en relation avec les luttes pratiques du mouvement communier multiséculaire, il dé-crypte le devenir du temps long dans une série de Commentaires où la pensée radicale permet à la fois de démontrer et dé-monter le mensonge quotidien qui, dans chaque fait, laisse apparaître les déterminations et les forces productives de la vie fausse…<br><br>

                            En s’appuyant sur la tradition primordiale de la communauté de l’être telle qu’elle exprime la vie générique du cosmos non divisé par le travail aliénatoire des échanges, Francis Cousin nous invite à nous émanciper de la liberté despotique de la vérité inversée.<br><br>

                            Oui, ce sont bien les puissances ténébreuses de l’accumulation capitaliste qui sont partout à l’œuvre et ce sont elles que l’humanité tout entière doit affronter en s’appuyant sur une conscience d’insoumission enfin retrouvée pour que la négation spectaculaire de l’épanouissement humain soit niée à son tour.</td></tr>
                    <tr><td class="prix">21,00 €<br>En stock</td></tr>
                        </tbody></table>
            </div>
            <hr>
            <div id="plan">
                <h2>Bon plan du moment</h2>
                <table><tbody>
                    <tr><td class="résumé" rowspan="2">Le spectacle du fétichisme de la marchandise fait le devenir du monde. L’existence humaine n’y est là qu’une longue errance angoissée sur le marché narcissique des rencontres factices. Partout règne la liberté despotique de l’argent et l’humain asservi et déchiré par la dictature démocratique de l'avoir et du paraître, ne cesse de consommer sa propre soumission. Contre ce totalitarisme de la fausse conscience, il s’agit de tourner le dos à la mise en scène de la passivité moderne, pour retrouver les véritables chemins du sens critique et poser en pratique la question radicale de l'authenticité de l'être.<br><br>

                            L’auteur, Francis Cousin est docteur en philosophie. Il a produit ou participé depuis trente ans à de nombreuses productions, essentiellement dans des cadres collectifs ou anonymes, parce qu’il récuse la possibilité d’approcher la vérité réelle, forcément impersonnelle, à travers la perception vaniteuse de l’intellectuel égotiste. Il se définit lui-même comme philosophe praticien du logos radical. Son projet vise à dé-voiler la nature des mystifications historiques en explicitant la lutte ontologique universelle entre l’être générique de l’homme, et les progrès civilisateurs de la domestication.<br><br>

                            Au cœur de sa démarche, Francis Cousin a fondé à Paris le Cabinet de <span class="fort">Philo-analyse</span>, un lieu de dia-logue qui accompagne tous ceux qui entendent – par la dynamique de la parole <span class="fort">dé-liée</span>, retrouver l’authenticité désaliénée du cheminement vers un vivre humain véritable. Bref, sortir de l’ordre psychique contemporain du mensonge généralisé, dont la seule finalité est d’amener l’humain à se renier lui-même.<br><br>

                            À l'heure où les troubles sociaux d’envergure, qui partout s’annoncent, menacent l’organisation inhumaine de l’ordre existant, l’auteur tient à dire qu’il n’est pas indifférent de rappeler que toutes les politiques de la raison marchande sont, de l’extrême droite à l’extrême gauche du Capital, l’ennemi absolu et définitif de toute joie humaine véridique.</td>
                        <td class="image"><a href="produit.php?produit=2">
                                <img src="images/l-etre-contre-l-avoir-cote.png" alt="Bon plan du moment"></a></tr>
                    <tr><td class="prix">21,00 €<br>En stock</td></tr>
                    </tbody></table>
            </div>
            <hr>
            <div id="fav">
                <h2>Nos coups de coeurs</h2>
                <table><tbody>
                    <tr><td><a href="produit.php?produit=7">
                    <img src="images/a.png" alt="Nos coups de coeurs 1">
                </a></td>
                    <td><a href="produit.php?produit=8">
                    <img src="images/a1.png" alt="Nos coups de coeurs 2">
                </a></td>
                    <td><a href="produit.php?produit=9">
                    <img src="images/a2.png" alt="Nos coups de coeurs 3">
                </a></td>
                    <td><a href="produit.php?produit=10">
                    <img src="images/a3.png" alt="Nos coups de coeurs 4">
                </a></td>
                    <td><a href="produit.php?produit=11">
                    <img src="images/a4.png" alt="Nos coups de coeurs 5">
                </a></td>
                </tr>
                    <tr>
                        <td><strong>21,00 €<br />En stock</strong></td>
                        <td><strong>21,00 €<br />En stock</strong></td>
                        <td><strong>22,00 €<br />En stock</strong></td>
                        <td><strong>26,00 €<br />En stock</strong></td>
                        <td><strong>16,00 €<br />En stock</strong></td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
    <footer>
        <div id="contact">
            <a href="accueil.php">
                <img src="images/titlefooter.png" alt="Url du site"></a>
            <a href="https://www.facebook.com">
                <img src="images/facebook.png" alt="Url facebook"></a>
            <a href="https://www.twitter.com">
                <img src="images/twitter.png" alt="Url twitter"></a>
            <a href="https://www.youtube.com">
                <img src="images/youtube.png" alt="Url youtube"></a>
            <a href="https://www.linkedin.com">
                <img src="images/linkledin.png" alt="Url linkledin"></a>
            <a href="https://plus.google.com">
                <img src="images/google+.png" alt="Url google+"></a>
            <img src="images/rss.png" alt="Url rss">
            <p><a href="mailto:pierre.barthelemy@etu.univ-rouen.fr,mathieu.blondel@etu.univ-rouen.fr?subject=Contact%20administrateurs%20du%20site%20&body=Bonjour,%20je%20souhaite%20vous%20contacter%20au%20sujet%20de">
                    Nous contacter</a> | Téléphone : 02.35.92.34.75 | 76801 Saint-Étienne-du-Rouvray<br /><br />
                À propos du site | CGU & Politique de confidentialité |
                <a href="admin.php">Administration du site</a>
            </p>
        </div>
    </footer>
<script>
    var slideIndex = 1;
    showDivs(slideIndex);

    function currentDiv(n) {
        showDivs(slideIndex = n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        if (n > x.length) {slideIndex = 1}
        if (n < 1) {slideIndex = x.length}
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
        }
        x[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " w3-opacity-off";
    }
</script>
</body>
</html>
